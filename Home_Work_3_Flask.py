from flask import Flask
from faker import Faker

app = Flask(__name__)
fake = Faker()


@app.route('/requirements')
def get_requirements():
    requirements = open('requirements.txt')
    res = requirements.read()
    return res


@app.route('/random_users')
def gen_random_users():
    file_w = open('random_users.txt', 'w')
    for _ in range(100):
        user = fake.email() + ' ' + fake.name() + '<br>'
        file_w.write(user)
    file_w.close()

    lst_of_users = open('random_users.txt')
    lst = lst_of_users.read()
    return lst


@app.route('/avr_data')
def get_avr_data():
    students = open('hw.csv')
    counter = 0
    height = 0
    weight = 0
    while True:
        lst_std = students.readline()
        if lst_std == '':
            break
        if counter == 0:
            counter += 1
            continue
        std_lst = lst_std.split(', ')
        height += float(std_lst[1])
        weight += float(std_lst[2])
        counter += 1

    counter -= 1
    medium_height = str(height / counter)
    medium_weight = str(weight / counter)

    return medium_height + ' ' + medium_weight


if __name__ == '__main__':
    app.run(host='127.0.0.1', port=5000)
